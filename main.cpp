#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include <sstream>
#include <cassert>

using digit = char;

void getFirstUniqueDigits(uint32_t a, uint32_t b,
                          digit& dif_a, digit& dif_b)
{
    std::string str_a = std::to_string(a);
    std::string str_b = std::to_string(b);

    size_t len_a = str_a.size();
    size_t len_b = str_b.size();
    for (size_t i = 0; i < len_a + len_b ; ++i) {
        if (str_a[i % len_a] != str_b[i % len_b]) {
            dif_a = str_a[i % len_a];
            dif_b = str_b[i % len_b];
            return;
        }
    }
}

bool myCompare(uint32_t a, uint32_t b)
{
    if (a == b) {
        return false;
    }
    digit dif_a = 0;
    digit dif_b = 0;
    getFirstUniqueDigits(a, b, dif_a, dif_b);

    return dif_a > dif_b;
}

std::string maxCombinedNumber(const std::vector<uint32_t>& in)
{
    std::vector<uint32_t> vec(in);
    std::sort(vec.begin(), vec.end(), myCompare);

    std::stringstream ss;
    for(auto& x: vec) {
        ss << x;
    }
    return ss.str();
}

void test(const std::vector<uint32_t>& in, const std::string& expected)
{
    std::cout << maxCombinedNumber(in) << std::endl;
    assert(maxCombinedNumber(in) == expected);
}

int main()
{

    std::vector<uint32_t> in1 = {1, 2, 3, 4, 50, 6};
    test(in1, "6504321");

    std::vector<uint32_t> in2 = {65, 67, 6, 66, 68, 69, 456};
    test(in2, "69686766665456");

    std::vector<uint32_t> in3 = {1, 0, 01, 11, 101, 110, 111, 1010};
    test(in3, "111111111010110100");

    std::vector<uint32_t> in4 = {46, 464};
    test(in4, "46464");

    std::vector<uint32_t> in5 = {43, 434};
    test(in5, "43443");

    std::vector<uint32_t> in6 = {4567, 45678};
    test(in6, "456784567");

    std::vector<uint32_t> in7 = {4567, 45672};
    test(in7, "456745672");

    std::vector<uint32_t> in8 = {7654, 76543};
    test(in8, "765476543");

    std::vector<uint32_t> in9 = {7654, 76549};
    test(in9, "765497654");

    std::vector<uint32_t> in10 = {901, 9, 98, 9800};
    test(in10, "9989800901");

    std::vector<uint32_t> in11 = {436, 4365};
    test(in11, "4365436");

    return 0;
}
