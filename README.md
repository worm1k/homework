# homework

## build
The task was written on Fedora 28, using gnu gcc 8.2.1  
build command:
```
g++ -o run main.cpp
```
run tests with:
```
./run
```
## Explanation

We sort all numbers from the input vector and then join them consequentially.  
The idea is to write the correct compare function for std::sort.  

To get the highest number,  
we want to place the number from list with the highest first digit  
in the result number's highest rank.  
If two input numbers have equal first digits,  
find first unique digits and compare them.  

When we search for the unique digits, we iterate through the number's  
digits in a cyclic manner. But not more than `n + m` steps, where  
`n` and `m` are amount of digits in each number respectively.  
If there are no unique digits within `n + m` iterations, numbers are  
equal cyclically

